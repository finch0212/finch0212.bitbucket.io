let things = document.querySelectorAll(".thing");

function r(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function func() {
  things.forEach((thing) => {
    let r1 = r(40, 60);
    let r2 = r(40, 60);
    let r3 = r(40, 60);
    let r4 = r(40, 60);

    thing.style.width = `${r(450, 500)}px`;
    thing.style.height = `${r(450, 500)}px`;
    thing.style.backgroundImage = `linear-gradient(270deg, blue, red)`;
    thing.style.filter = `hue-rotate(${r(0, 360)}deg)`;
    thing.style.borderRadius = `${r1}% ${100 - r1}% ${r2}% ${100 - r2}% 
    / ${r3}% ${r4}% ${100 - r4}% ${100 - r3}%`;
  });
}

setTimeout(func, 100);
setInterval(func, 3000);
