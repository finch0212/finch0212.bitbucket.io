import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react'


/* Входные параметры конфигурации */
type ConfigProps = {
  /* Режим запуска приложения */
  mode: 'production' | 'development'
}

/* Конфигурация приложения */
export default (props: ConfigProps) => {
  /* Перенос параметров конфигурации в текущий процесс */
  process.env = {...process.env, ...loadEnv(props.mode, process.cwd(), '')};

  return defineConfig({
    plugins: [react()],
    server: {
      // proxy: {
      //   '/asdt-manager/api/': `http://${process.env.BACKEND_HOST || 'localhost'}:${process.env.BACKEND_PORT || 8088}`,
      // },
      //port: +process.env.FRONTEND_PORT || 8084,
      //open: true,
    },
    preview: {
      port: 3184,
    },
    base: '/the-app/',
    build: {
      target: "es2015",
      assetsInlineLimit: 5000,
      outDir: "../",
      rollupOptions: {
        output: {
          entryFileNames: `assets/[name].js`,
          chunkFileNames: `assets/[name].js`,
          assetFileNames: 'assets/[name].[ext]'
        }
      }
    },
  });
}