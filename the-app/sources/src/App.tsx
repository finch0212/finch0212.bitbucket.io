import logo from './k.png'
import './App.css'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Hello Vite + React + Bitbucket Pages + Kirill!</p>
      </header>
    </div>
  )
}

export default App
