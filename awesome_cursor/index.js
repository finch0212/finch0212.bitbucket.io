const onClick = () => {
    alert('onClick');
}

let cursor = document.querySelector(".cursor");
let img = document.querySelector("img");
let navLinks = document.querySelectorAll(".link");

window.addEventListener('mousemove', cursorF)

function cursorF (e) {
    cursor.style.top = `${e.pageY}px`;
    cursor.style.left = `${e.pageX}px`;
}

navLinks.forEach(link => {
    link.addEventListener('mouseover', () => {
        cursor.classList.add('link-grow-black');
        cursor.style.zIndex = '-1';
        link.style.color = 'white';
    });
    link.addEventListener('mouseleave', () => {
        cursor.classList.remove('link-grow-black');
        cursor.style.zIndex = '99999';
        link.style.color = 'black';
    });
});

img.addEventListener('mouseover', () => {
    cursor.classList.add('link-grow');
    cursor.style.backdropFilter = 'saturate(3) contrast(2) hue-rotate(180deg)';
});
img.addEventListener('mouseleave', () => {
    cursor.classList.remove('link-grow');
    cursor.style.backdropFilter = 'none';
});